resource "aws_ec2_transit_gateway_route_table" "attachment_route_table" {

  for_each = var.accept_vpc_attachments

  transit_gateway_id = aws_ec2_transit_gateway.this.id

  tags = merge(local.tags, {
    Name = "tgw-${each.key}-route-table-tflz"
  })
}

resource "aws_ec2_transit_gateway_route" "routes" {

  for_each = local.all_vpc_attachment_routes

  destination_cidr_block         = each.value.cidr
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.attachment_route_table[each.value.unique_attachment_identifier].id
  transit_gateway_attachment_id  = each.value.attachment_id
}



resource "aws_ec2_transit_gateway_route_table_association" "rt-associations" {
  for_each = var.accept_vpc_attachments

  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpc_attachment.vpc_attachments[each.key].id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.attachment_route_table[each.key].id

}
