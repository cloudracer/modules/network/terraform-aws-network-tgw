data "aws_ec2_transit_gateway_vpc_attachment" "vpc_attachments" {
  for_each = var.accept_vpc_attachments

  filter {
    name   = "vpc-id"
    values = [each.value.vpc_id]
  }

  filter {
    name   = "state"
    values = ["available", "pendingAcceptance", "pending", "initiatingRequest"]
  }

}

resource "aws_ec2_transit_gateway_vpc_attachment_accepter" "vpc_attachment_accepter" {
  for_each = var.accept_vpc_attachments

  transit_gateway_attachment_id = data.aws_ec2_transit_gateway_vpc_attachment.vpc_attachments[each.key].id

  # Should be true by default, otherwise the accepter is sad.
  transit_gateway_default_route_table_association = lookup(
    each.value, "default_route_table_association", false
  )

  # Propagation is typically not what we want by default.
  transit_gateway_default_route_table_propagation = lookup(
    each.value, "default_route_table_propagation", false
  )

  tags = merge(local.tags, {
    Name = "tgw-attach-${each.key}-tflz"
  })


}
