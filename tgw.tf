# Create Transit Gateway
resource "aws_ec2_transit_gateway" "this" {
  description = "Main Transit Gateway for region: ${data.aws_region.current.name}"

  amazon_side_asn                 = var.amazon_side_asn
  vpn_ecmp_support                = var.vpn_ecmp_support
  auto_accept_shared_attachments  = var.auto_accept_shared_attachments == true ? "enable" : "disable"
  default_route_table_association = "disable"
  default_route_table_propagation = "disable"

  tags = merge(local.tags, {
    Name = lower("tgw-network-${data.aws_region.current.name}-tflz")
  })
}
