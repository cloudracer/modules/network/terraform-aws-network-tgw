locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-network-transit-gateway"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-network-transit-gateway"
  }
  tags = merge(local.tags_module, var.tags)

  list_of_all_vpc_attachment_routes = flatten([for id, config in var.accept_vpc_attachments : config.routes])

  all_vpc_attachment_routes = { for index, value in local.list_of_all_vpc_attachment_routes : index => value }
}
