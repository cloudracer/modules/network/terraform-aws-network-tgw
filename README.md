# Terraform Module for AWS Transit Gateway

This module will create the required AWS resources for deploying an AWS Transit Gateway which is required
to connect other VPCs or VPN connections.

Its part of a multi-stage network concept and designed to work with terraform workspaces.
For a complete network setup, you have to include additional modules.

- VPC : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpc.git
- Transit Gateway : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-transit-gateway.git
- VPN : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpn.git
- Route53 : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-route53.git
- Direct Connect : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-direct-connect.git

***CAVE***: It's strongly required to name the terraform workspace & the shared vpc: ***shared***
This is currently a hard coded value in all modules.

## Features
#### Routing
- Static routes or BGP

## Examples

    module "euc1_transit_gateway" {

        source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-transit-gateway.git?ref=master"

        providers = {
            aws = aws.eu-central-1
        }

        name      = local.general.name
        workspace = terraform.workspace

        enable_static_routing             = local.vpn_settings.euc1.enable_static_routing
        vpn_routing_cidrs                 = local.vpn_cidrs.euc1.datacenter
        vpn_transit_gateway_attachment_id = module.euc1_vpn.transit_gateway_attachment_id

        vpc_id          = module.euc1_vpc.vpc_id
        vpc_cidr        = local.vpc_euc1_primary_cidr
        vpc_cidr_shared = local.stages.euc1.primary.shared
        private_subnets = module.euc1_vpc.private_subnets

        tags = local.common_tags
    }

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ec2_transit_gateway.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway) | resource |
| [aws_ec2_transit_gateway_route.routes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route) | resource |
| [aws_ec2_transit_gateway_route_table.attachment_route_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route_table) | resource |
| [aws_ec2_transit_gateway_route_table_association.rt-associations](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route_table_association) | resource |
| [aws_ec2_transit_gateway_vpc_attachment_accepter.vpc_attachment_accepter](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_vpc_attachment_accepter) | resource |
| [aws_ram_principal_association.tgw_share_principal_association](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_principal_association) | resource |
| [aws_ram_resource_association.tgw_share_resource_association](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_association) | resource |
| [aws_ram_resource_share.tgw_share](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_share) | resource |
| [aws_ec2_transit_gateway_vpc_attachment.vpc_attachments](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ec2_transit_gateway_vpc_attachment) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_accept_vpc_attachments"></a> [accept\_vpc\_attachments](#input\_accept\_vpc\_attachments) | List of VPCs from which we accept attachment requests. | `map(any)` | `{}` | no |
| <a name="input_amazon_side_asn"></a> [amazon\_side\_asn](#input\_amazon\_side\_asn) | The CIDR blocks which will be routed into a blackhole | `number` | `64512` | no |
| <a name="input_auto_accept_shared_attachments"></a> [auto\_accept\_shared\_attachments](#input\_auto\_accept\_shared\_attachments) | Whether resource attachment requests are automatically accepted. Valid values: true / false. | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | Name to be used on all the resources as identifier | `string` | `""` | no |
| <a name="input_share_with_accounts"></a> [share\_with\_accounts](#input\_share\_with\_accounts) | The list of Account IDs this Transit Gateway should be shared with | `list(string)` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to add to all resources | `map(string)` | `{}` | no |
| <a name="input_vpn_ecmp_support"></a> [vpn\_ecmp\_support](#input\_vpn\_ecmp\_support) | Whether VPN Equal Cost Multipath Protocol support is enabled. Valid values: disable, enable. Default value: enable. | `string` | `"enable"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_transit_gateway_id"></a> [transit\_gateway\_id](#output\_transit\_gateway\_id) | Transit Gateway ID for the current environment |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
