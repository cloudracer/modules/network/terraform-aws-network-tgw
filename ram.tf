resource "aws_ram_resource_share" "tgw_share" {
  count = length(var.share_with_accounts) > 0 ? 1 : 0

  name = lower("tgw-network-${data.aws_region.current.name}-share-tflz")

  tags = merge(local.tags, {
    Name = lower("tgw-network-${data.aws_region.current.name}-share-tflz")
  })
}

resource "aws_ram_principal_association" "tgw_share_principal_association" {
  count = length(var.share_with_accounts)

  principal = element(var.share_with_accounts, count.index)

  resource_share_arn = aws_ram_resource_share.tgw_share.0.arn

}

resource "aws_ram_resource_association" "tgw_share_resource_association" {

  count = length(var.share_with_accounts) > 0 ? 1 : 0

  resource_arn       = aws_ec2_transit_gateway.this.arn
  resource_share_arn = aws_ram_resource_share.tgw_share.0.arn
}
