## General
variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = ""
}

## TGW Info
variable "amazon_side_asn" {
  description = "The CIDR blocks which will be routed into a blackhole"
  type        = number
  default     = 64512
}

variable "auto_accept_shared_attachments" {
  description = "Whether resource attachment requests are automatically accepted. Valid values: true / false."
  type        = bool
  default     = false
}

variable "vpn_ecmp_support" {
  description = "Whether VPN Equal Cost Multipath Protocol support is enabled. Valid values: disable, enable. Default value: enable."
  type        = string
  default     = "enable"
}



# variable "enable_static_routing" {
#   description = "Whether the VPN connection uses static routes exclusively. Valid values: true, false. Default value: false."
#   type        = bool
#   default     = false
# }

# variable "vpn_routing_cidrs" {
#   description = "The CIDR blocks associated with the local subnet of the customer network"
#   type        = list(string)
#   default     = []
# }

# variable "vpn_transit_gateway_attachment_id" {
#   description = "The current AWS Transit Gateway Attachment ID"
#   type        = string
#   default     = ""
# }

## Other
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "share_with_accounts" {
  description = "The list of Account IDs this Transit Gateway should be shared with"
  type        = list(string)
  default     = []
}

variable "accept_vpc_attachments" {
  description = "List of VPCs from which we accept attachment requests."
  type        = map(any)
  default     = {}
}
